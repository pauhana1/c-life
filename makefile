CC=gcc
CFLAGS=-c -std=c89 -g3 -DDEBUG
INCLUDES= -I../.

SRCS = c-life.c
OBJS = $(SRCS:.c=.o)
MAIN = clife

.PHONY: depend clean

all:    $(MAIN)
	@echo  $(MAIN) Compiled

$(MAIN): $(OBJS)
	$(CC) $(INCLUDES) -o $(MAIN) $(OBJS)

# this is a suffix replacement rule for building .o's from .c's
# it uses automatic variables $<: the name of the prerequisite of
# the rule(a .c file) and $@: the name of the target of the rule (a .o file)
# (see the gnu make manual section about automatic variables)
.c.o:
	$(CC) $(CFLAGS) $(INCLUDES) $<  -o $@

clean:
	$(RM) *.o *~ $(MAIN)

depend: $(SRCS)
	makedepend $(INCLUDES) $^

# DO NOT DELETE THIS LINE -- make depend needs it
